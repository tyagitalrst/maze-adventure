extends Area2D

export var damage_point = 0
export var heal_point = 0
onready var getPlayer = $"../Player"

func hit_player():
	getPlayer.damage(damage_point)

func heal_player():
	getPlayer.heal(heal_point)
	

func _on_Spike_body_entered(body):
	if body.get_name() == "Player":
		hit_player()
	
		
func _on_Damage_body_exited(body):
	if body.get_name() == "Player":
		$AnimatedSprite.stop()
		

func _on_Health_Mushroom_body_entered(body):
	if body.get_name() == "Player":
		$AnimatedSprite.play("default")
		heal_player()
		heal_point = 0
		$AnimatedSprite.hide()


func _on_Bomb_body_entered(body):
	if body.get_name() == "Player":
		$AnimatedSprite.play("default")
		hit_player()
		damage_point = 0
		$AnimatedSprite.hide()


func _on_Damage_body_entered(body):
	if body.get_name() == "Player":
		$AnimatedSprite.play("default")
		hit_player()
