extends KinematicBody2D

export (int) var enemy_speed = 40
export var enemy_motion = Vector2()
export (int) var GRAVITY = 10

var UP = Vector2(0, -1)
var enemy_direction = 1
var enemy_flip_direction = -1


func _ready():
	set_physics_process(true)

func _physics_process(delta):
	
	enemy_motion.y += GRAVITY
	
	if is_on_wall() or $RayCast2D.is_colliding() == false :
		enemy_direction = enemy_direction * enemy_flip_direction
		$RayCast2D.scale.x *= -1
		
	if enemy_direction == 1:
		$Sprite.flip_h = false

	elif enemy_direction == -1:
		$Sprite.flip_h = true
		$RayCast2D.scale.x *= -1
		
	$Sprite.play("run")	
	enemy_motion.x = enemy_direction * enemy_speed
	enemy_motion = move_and_slide(enemy_motion, UP)
		

