extends KinematicBody2D

export(String) var scene_to_load

export (int) var speed = 300
export (int) var duck_speed = 200
export (int) var jump_speed = -600
export (int) var dash_speed = 600
export (float) var max_health = 100
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)
var velocity = Vector2()
var on_ground = false
var has_double_jumped = false

onready var health = max_health 

signal health_updated(health)
signal killed()

func teleport_to(targetPos):
	position = targetPos
	
func _ready():
	$Textbox.show()
	
func get_input():	
	velocity.x = 0
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		$Sprite.flip_h = false
		$Textbox.hide()
		$Sprite.play('walk')
	elif Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		$Sprite.flip_h = true
		$Textbox.hide()
		$Sprite.play('walk')
	else:
		$Sprite.play('idle')
		
		
	if Input.is_action_pressed('ui_right') && Input.is_action_pressed("ui_select"):
		velocity.x += dash_speed
		$Sprite.flip_h = false
		$Sprite.play('dash')
	elif Input.is_action_pressed('ui_left') && Input.is_action_pressed("ui_select"):
		velocity.x -= dash_speed
		$Sprite.flip_h = true
		$Sprite.play('dash')
		
	on_floor()	
	
	if Input.is_action_just_pressed('ui_up'):
		if on_ground == true:
			velocity.y = jump_speed 
			has_double_jumped = false
			on_ground = false
		elif on_ground == false and has_double_jumped == false:
			velocity.y = jump_speed
			has_double_jumped = true
	
	if Input.is_action_pressed('ui_right') && Input.is_action_pressed("ui_down"):
		velocity.x += duck_speed
		$Sprite.flip_h = false
		$Sprite.play('duck')
	elif Input.is_action_pressed('ui_left') && Input.is_action_pressed("ui_down"):
		velocity.x -= duck_speed
		$Sprite.flip_h = true
		$Sprite.play('duck')


func on_floor():
	if is_on_floor():
		on_ground = true
	else:
		on_ground = false
		$Sprite.play('jump')
		
	
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	
func damage(amount):
	_set_health(health - amount)
	
func heal(amount):
	_set_health_heal(health + amount)
	
func kill():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))	

func _set_health(value):
	health = value
	emit_signal("health_updated", health)
	if health <= 0:
		kill()
		emit_signal("killed")

func _set_health_heal(value):
	health = value
	emit_signal("health_updated", health)
	if health >= 100:
		health = max_health
		emit_signal("health_updated", health)
	
	



