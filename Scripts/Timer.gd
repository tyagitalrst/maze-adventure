extends Node2D

export(String) var scene_to_load

onready var time_label = $"CanvasLayer/GUI/TimerLabel"
onready var game_timer = $GameTimer



func _process(delta):
	time_label.text = "Timer: " + str(int(game_timer.time_left))


func _on_GameTimer_timeout():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
